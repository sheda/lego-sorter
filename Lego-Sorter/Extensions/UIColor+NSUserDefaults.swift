//
//  UIColor+NSUserDefaults.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 9/7/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults {
  
  func setColor(value: UIColor?, forKey: String) {
    guard let value = value else {
      set(nil, forKey: forKey)
      return
    }
    set(NSKeyedArchiver.archivedData(withRootObject: value), forKey: forKey)
  }
  
  func colorForKey(key:String) -> UIColor? {
    guard let data = object(forKey: key), let color = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? UIColor else {
      return nil
    }
    return color
  }
  
}
