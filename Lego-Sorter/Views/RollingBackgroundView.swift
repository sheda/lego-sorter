//
//  RollingBackgroundView.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/22/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit

class RollingBackgroundView: UIView {

  var points : [CGPoint] = []
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  convenience init(frame: CGRect, points: [CGPoint]) {
    self.init(frame: frame)
    self.points = points
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    let path1 = getPath(rect: rect)
    
    //set stroke style
    UIColor.lightGray.setStroke()
    path1.lineCapStyle = .round
    path1.lineJoinStyle = .round
    path1.lineWidth = rect.height/10 + 1
    path1.stroke()
    
    let context = UIGraphicsGetCurrentContext()
    let path = getPath(rect: rect)
    
    //create shadow of path
    let shadow = UIColor(displayP3Red: 216/255, green: 208/255, blue: 208/255, alpha: 0.5)
    let shadowOffset = CGSize(width: 4, height: 4)
    let shadowBlurRadius: CGFloat = 22
    context!.saveGState()
    context!.setShadow(offset: shadowOffset, blur: shadowBlurRadius, color: shadow.cgColor)
    
    //et inner stroke style
    UIColor.white.setStroke()
    path.lineCapStyle = .round
    path.lineJoinStyle = .round
    path.lineWidth = rect.height/10
    path.stroke()
    context?.restoreGState()
  }
  
  //draw path
  func getPath(rect: CGRect)->UIBezierPath{
    let path = UIBezierPath()
    if points.count > 0{
      path.move(to: CGPoint(x: points[0].x, y: points[0].y))
      for i in 1...(points.count - 1) {
        path.addLine(to: CGPoint(x: points[i].x, y: points[i].y))
      }
    }
    return path
  }
}
