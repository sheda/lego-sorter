//
//  RandomMovingCirclesView.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/24/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit

class RandomMovingCirclesView: UIView {

  var points : [UIView] = []
  var colors = [
    UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1),
    UIColor(red: 220/255, green: 210/255, blue: 28/255, alpha: 1),
    UIColor(red: 108/255, green: 188/255, blue: 20/255, alpha: 1),
    UIColor(red: 0/255, green: 91/255, blue: 198/255, alpha: 1),
    UIColor(red: 190/255, green: 20/255, blue: 20/255, alpha: 1)
  ]

  func randomInt(min: Int, max:Int) -> Int {
    return min + Int(arc4random_uniform(UInt32(max - min + 1)))
  }
  
  override func awakeFromNib() {
    initScreen()
  }
  
  func initScreen(){
    for v in points{
      v.layer.removeAllAnimations()
      v.removeFromSuperview()
    }
    let count = randomInt(min: 5, max: 8)
    for _ in (0 ... count){
      let x = randomInt(min: 0, max: Int(bounds.width))
      let y = randomInt(min: 0, max: Int(bounds.height))
      let size = randomInt(min: 10, max: 70)
      let circle = UIView(frame: CGRect(x: x, y: y, width: size, height: size))
      circle.layer.cornerRadius = CGFloat(size / 2)
      circle.backgroundColor = colors[randomInt(min: 0, max: colors.count - 1)]
      points.append(circle)
      addSubview(circle)
      UIView.animate(withDuration: TimeInterval(randomInt(min: 1, max: 5)), delay: TimeInterval(randomInt(min: 0, max: 3)), options: [.repeat, .autoreverse], animations: {
        circle.transform = CGAffineTransform(scaleX: 2, y: 2)
      }, completion: nil)
    }
  }
}
