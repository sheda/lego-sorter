//
//  FinalSplashViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/24/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit

class FinalSplashViewController: UIViewController {

  @IBOutlet weak var circleBackgroundView: RandomMovingCirclesView!
  
  override func viewWillAppear(_ animated: Bool) {
    circleBackgroundView.initScreen()
  }
  
  override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
          self.performSegue(withIdentifier: "start", sender: nil)
        })
    }
}
