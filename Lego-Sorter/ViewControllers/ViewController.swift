//
//  ViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/21/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

  @IBOutlet weak var motorSegment: UISegmentedControl!
  @IBOutlet weak var colorSegment: UISegmentedControl!
  var ref: DatabaseReference!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    ref = Database.database().reference()
    ref.child("sorter").observeSingleEvent(of: .value, with: { (snapshot) in
      if let value = snapshot.value as? NSDictionary{
        if let color = value["color"] as? Int {
          self.colorSegment.selectedSegmentIndex = color
        }
        if let motor = value["motor"] as? Int {
          self.motorSegment.selectedSegmentIndex = motor
        }
      }
    }) { (error) in
      print(error.localizedDescription)
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func onMotorSegmentChanged(_ sender: Any) {
    let color = motorSegment.selectedSegmentIndex
    self.ref.child("sorter").updateChildValues(["motor": color])
  }
  
  @IBAction func onSegmentChanged(_ sender: Any) {
    let color = colorSegment.selectedSegmentIndex
    self.ref.child("sorter").updateChildValues(["color": color])
  }
  
}
