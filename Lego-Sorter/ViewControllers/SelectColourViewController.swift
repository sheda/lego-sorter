//
//  SelectColourViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/21/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SelectColourViewController: UIViewController, UIGestureRecognizerDelegate {

  @IBOutlet weak var topTextLabels: UIView!
  @IBOutlet weak var bottomLabel: UILabel!
  @IBOutlet weak var confirmButton: UIButton!
  
  @IBOutlet weak var dialContainerView: UIView!
  @IBOutlet weak var selectColourContainer: UIView!
  var rainbowCircle : UIView!
  var dial : UIView! = UIView()
  var middleLabel : UILabel!
  var colorText : String = ""
  
  var stroke : CGFloat = 70
  var hasSelected = false
  
  var color = UIColor.white
  
  var ref = Database.database().reference()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidLayoutSubviews() {
    rainbowCircle = RainbowCircle(frame: selectColourContainer.bounds, lineHeight: stroke)
    rainbowCircle.backgroundColor = .clear
    selectColourContainer.addSubview(rainbowCircle)
    
    let rbCWidth = rainbowCircle.bounds.width
    let rbCHeight = rainbowCircle.bounds.height
    let innerCircle = UIView(frame: CGRect(x: stroke + 2, y: stroke + 2, width: rbCWidth - (2 * stroke) - 4, height: rbCHeight - (2 * stroke) - 4))
    innerCircle.backgroundColor = .white
    innerCircle.layer.cornerRadius = innerCircle.bounds.width/2
    selectColourContainer.addSubview(innerCircle)
    
    middleLabel = UILabel(frame: innerCircle.bounds)
    middleLabel.textAlignment = .center
    middleLabel.isHidden = true
    middleLabel.font = UIFont(name:"WorkSans-Regular", size: 40)
    middleLabel.adjustsFontSizeToFitWidth = true
    middleLabel.minimumScaleFactor = 0.1
    innerCircle.addSubview(middleLabel)
    
    confirmButton.layer.cornerRadius = 25
    confirmButton.isHidden = true
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
    tap.delegate = self
    view.addGestureRecognizer(tap)
    
    let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
    pan.delegate = self
    view.addGestureRecognizer(pan)
  }
  
  func updateUI(hue : CGFloat){
    topTextLabels.isHidden = true
    bottomLabel.isHidden = true
    if ((0.05...0.13 ~= hue)) {
      colorText = "ORANGE"
      color = UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1)
      self.ref.child("sorter").updateChildValues(["color": 1])
    } else if (0.13...0.19 ~= hue){
      colorText = "YELLOW"
      color = UIColor(red: 220/255, green: 210/255, blue: 28/255, alpha: 1)
      self.ref.child("sorter").updateChildValues(["color": 2])
    } else if (0.19...0.43 ~= hue) {
      colorText = "GREEN"
      color = UIColor(red: 108/255, green: 188/255, blue: 20/255, alpha: 1)
      self.ref.child("sorter").updateChildValues(["color": 3])
    } else if (0.43...0.84 ~= hue) {
      colorText = "BLUE"
      color = UIColor(red: 0/255, green: 91/255, blue: 198/255, alpha: 1)
      self.ref.child("sorter").updateChildValues(["color": 4])
    } else if ((0.84...1 ~= hue) || (0...0.05 ~= hue)) {
      colorText = "RED"
      color = UIColor(red: 190/255, green: 20/255, blue: 20/255, alpha: 1)
      self.ref.child("sorter").updateChildValues(["color": 0])
    }
    middleLabel.text = "You’ve \n picked a \n \(colorText) \n M&M"
    middleLabel.isHidden = false
    confirmButton.isHidden = false
    middleLabel.numberOfLines = 0;
    confirmButton.setTitleColor(color, for: .normal)
    middleLabel.textColor = color
    view.backgroundColor = color
  }
  
  @objc func handleTap(sender: UIGestureRecognizer? = nil) {
    if sender != nil{
      let locx = (sender!.location(in: sender!.view).x) - view.bounds.width / 2
      let locy = (sender!.location(in: sender!.view).y) - view.bounds.height / 2
      let angle = atan2(locy,-locx) - (.pi / 2)
      let r = (dialContainerView.bounds.width/2) - (stroke/2)
      let x = (r * sin(angle) + (dialContainerView.bounds.width/2)) - stroke/2
      let y = (r * cos(angle) + (dialContainerView.bounds.height/2)) - stroke/2
      let hue = (atan2(locy,-locx) + .pi) / (2 * .pi)
      updateUI(hue: hue)
      
      dial.removeFromSuperview()
      dial = UIView(frame: CGRect(x: x, y: y, width: stroke, height: stroke))
      dial.backgroundColor = .clear
      dial.layer.borderWidth = 4
      dial.layer.borderColor = UIColor.white.cgColor
      dial.layer.cornerRadius = (stroke)/2
      dialContainerView.layer.zPosition = 99
      dial.layer.zPosition = 100
      dialContainerView.addSubview(dial)
    }
  }
  
  @IBAction func startButtonPressed(_ sender: Any) {
    performSegue(withIdentifier: "colorSelected", sender: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "colorSelected"{
      let dest = segue.destination as! RollingViewController
      let def = UserDefaults()
      def.setColor(value: color, forKey: "col")
      dest.setBallColor(color: color, name: colorText)
    }
  }

}
