//
//  FinishSplashViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/24/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit

class MiddleSplashViewController: UIViewController {

  @IBOutlet weak var circleBackgroundView: RandomMovingCirclesView!
  @IBOutlet weak var subscribeButton: UIButton!
  
  override func viewDidLoad() {
    subscribeButton.layer.cornerRadius = subscribeButton.bounds.height/2
  }
  
  override func viewWillAppear(_ animated: Bool) {
    circleBackgroundView.initScreen()
  }

}
