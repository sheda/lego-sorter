//
//  RollingViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/22/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit

class RollingViewController: UIViewController, UIGestureRecognizerDelegate {

  
  @IBOutlet weak var rollingBackgroundView: UIView!
  var ballColor = UserDefaults().colorForKey(key: "col") 
  var colorName = ""
  var ball = UIView()
  
  var points : [[CGPoint]] = []
  
  var height : CGFloat!
  var width : CGFloat!
  var ballWidth : CGFloat!
  
  var label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0 ))
  let labelTexts = ["COLOR M&M's rolling your way","The machine is now sorting M&Ms for you"]
  
  var animationNumber = 0
  
  func setBallColor(color: UIColor, name: String){
    ballColor = color
    colorName = name
  }
  
  
  override func viewDidLayoutSubviews() {
    height = rollingBackgroundView.bounds.height / 5
    width = rollingBackgroundView.bounds.width / 4
    ballWidth = height / 4
    
    label = createLabel(text: labelTexts[0].replacingOccurrences(of: "COLOR", with: colorName), x: 32, y: height)
    
    rollingBackgroundView.addSubview(label)
    
    let ballRadius = height / 4
    
    points = [
      [
        CGPoint(x: 0        , y:     height),
        CGPoint(x: width * 3, y:     height),
        CGPoint(x: width * 3, y: 2 * height),
        CGPoint(x: width * 2, y: 2 * height),
        CGPoint(x: width * 2, y: 3 * height),
        CGPoint(x: width * 1, y: 3 * height),
        CGPoint(x: width * 1, y: 4 * height),
        CGPoint(x: width * 4, y: 4 * height)
      ],
      [
        CGPoint(x: 0        , y: height * 5 / 2),
        CGPoint(x: width * 1, y: height * 5 / 2),
        CGPoint(x: width * 1, y: height),
        CGPoint(x: width * 3, y: height),
        CGPoint(x: width * 3, y: height * 5)
      ]
    ]
    
    let rollingBackground = RollingBackgroundView(frame: rollingBackgroundView.bounds, points: points[0])
    rollingBackground.backgroundColor = .clear
    rollingBackgroundView.addSubview(rollingBackground)
    
    ball.frame = CGRect(x: 0, y: height - ballRadius , width: ballRadius*2, height: ballRadius*2)
    ball.layer.cornerRadius = ballRadius
    ball.backgroundColor = ballColor
    rollingBackgroundView.addSubview(ball)
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
    tap.delegate = self
    rollingBackgroundView.addGestureRecognizer(tap)
  }
  
  @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
    if (sender != nil){
      view.layer.removeAllAnimations()
      self.performSegue(withIdentifier: "MMFinishedAnimate", sender: nil)
      
    }
  }
  
  func moveTo(point: CGPoint) -> CGRect{
    return CGRect(x: self.ball.frame.origin.x + point.x, y: self.ball.frame.origin.y + point.y, width: self.ball.frame.width, height: self.ball.frame.height)
  }

  override func viewDidAppear(_ animated: Bool) {
    firstAnimation()
  }
  
  func createLabel(text: String, x: CGFloat, y: CGFloat) -> UILabel{
    let tempLabel = UILabel(frame: CGRect(x: x, y: y, width: width, height: height*2))
    tempLabel.numberOfLines = 0
    tempLabel.font = UIFont(name: "WorkSans-SemiBold", size: 45)
    tempLabel.textColor = ballColor
    tempLabel.textAlignment = .center
    tempLabel.adjustsFontSizeToFitWidth = true
    tempLabel.minimumScaleFactor = 0.05
    tempLabel.text = text
    return tempLabel
  }
  
  func setUpSecondAnimation(){
    animationNumber = 1
    let rollingBackground = RollingBackgroundView(frame: rollingBackgroundView.bounds, points: points[1])
    rollingBackground.backgroundColor = .white
    rollingBackgroundView.addSubview(rollingBackground)
    
    ball.frame = CGRect(x: 0, y: rollingBackgroundView.bounds.height / 2 - self.ballWidth, width: self.ball.frame.width, height: self.ball.frame.height)
    
    ball.layer.cornerRadius = ballWidth
    ball.backgroundColor = ballColor
    rollingBackgroundView.addSubview(ball)
    
    label.removeFromSuperview()
    label = createLabel(text: labelTexts[1], x: 32, y: (height*3)-32)
    rollingBackgroundView.addSubview(label)
    
    secondAnimation()
  }
  
  func secondAnimation(){
    UIView.animate(withDuration: 0.5, delay:0, options: .curveEaseIn ,animations: {
      self.ball.frame = self.moveTo(point: CGPoint(x: self.width - self.ballWidth, y: 0))
    }) { (x) in
      UIView.animate(withDuration: 2, delay:0, options: .curveEaseIn, animations: {
        self.ball.frame = self.moveTo(point: CGPoint(x: 0, y: -self.height*1.5))
      }) { (x) in
        UIView.animate(withDuration: 1.5, delay:0, options: .curveEaseIn, animations: {
          self.ball.frame = self.moveTo(point: CGPoint(x: self.width * 2, y: 0))
        }) { (x) in
          UIView.animate(withDuration: 3, delay:0, options: .curveEaseIn, animations: {
            self.ball.frame = self.moveTo(point: CGPoint(x: 0, y: self.height * 4))
          }) { (x) in
            self.performSegue(withIdentifier: "MMFinishedAnimate", sender: nil)
          }
        }
      }
    }
  }
  
  func firstAnimation(){
    UIView.animate(withDuration: 4, delay:0, options: .curveEaseIn ,animations: {
      self.ball.frame = self.moveTo(point: CGPoint(x: 3*self.width - self.ballWidth, y: 0))
    }) { (x) in
      UIView.animate(withDuration: 2, delay:0, options: .curveEaseIn, animations: {
        self.ball.frame = self.moveTo(point: CGPoint(x: 0, y: self.height))
      }) { (x) in
        UIView.animate(withDuration: 2, delay:0, options: .curveEaseIn, animations: {
          self.ball.frame = self.moveTo(point: CGPoint(x: -self.width, y: 0))
        }) { (x) in
          UIView.animate(withDuration: 2, delay:0, options: .curveEaseIn, animations: {
            self.ball.frame = self.moveTo(point: CGPoint(x: 0, y: self.height))
          }) { (x) in
            UIView.animate(withDuration: 2, delay:0, options: .curveEaseIn, animations: {
              self.ball.frame = self.moveTo(point: CGPoint(x: -self.width, y: 0))
            }) { (x) in
              UIView.animate(withDuration: 2, delay:0, options: .curveEaseIn, animations: {
                self.ball.frame = self.moveTo(point: CGPoint(x: 0, y: self.height))
              }) { (x) in
                UIView.animate(withDuration: 4, delay:0, options: .curveEaseIn, animations: {
                  self.ball.frame = self.moveTo(point: CGPoint(x: 3*self.width + self.ballWidth, y: 0))
                }) { (x) in
                  self.setUpSecondAnimation()
                }
              }
            }
          }
        }
      }
    }
  }
  
}
