//
//  InputDetailsViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/23/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit
import FirebaseDatabase

class InputDetailsViewController: UIViewController {
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var circleBackgroundView: RandomMovingCirclesView!
  
  override func viewWillAppear(_ animated: Bool) {
    circleBackgroundView.initScreen()
  }
  
  @IBAction func submitButtonPressed(_ sender: Any) {
    //push emails
    if let email = emailTextField.text, let name = nameTextField.text{
      let ref = Database.database().reference()
      ref.child("users").updateChildValues([name: email])
      performSegue(withIdentifier: "detailsSubmitted", sender: nil)
    }
  }
}
