//
//  LandingPageViewController.swift
//  Lego-Sorter
//
//  Created by Lachi Agnew on 8/21/18.
//  Copyright © 2018 Lachlan Agnew. All rights reserved.
//

import UIKit

class LandingPageViewController: UIViewController, UIGestureRecognizerDelegate {

  @IBOutlet weak var dropButtonView: UIView!
  @IBOutlet weak var goView: UIView!
  @IBOutlet weak var goLabel: UILabel!
  @IBOutlet weak var tapImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    goView.layer.cornerRadius = 90
    dropButtonView.layer.cornerRadius = 90
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
    tap.delegate = self
    goView.addGestureRecognizer(tap)
  }
  
  @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
    goLabel.isHidden = true
    tapImage.isHidden = true
    
    UIView.animate(withDuration: 0.2, animations: {
      self.dropButtonView.transform = CGAffineTransform(translationX: 0, y: -28)
    }) { (x) in
      UIView.animate(withDuration: 0.5, animations: {
        self.goView.transform = CGAffineTransform(scaleX: 12, y: 12)
      }) { (x) in
        self.performSegue(withIdentifier: "col-select", sender: nil)
      }
    }
  }
}
