# LegoSorter iOS App

This is an iOS app the controls a IoT device used to sort M&Ms by colour. It uses OpenCV, full blog about this available at

[Smart Smarties: How to sort M&Ms using Internet of Things, Raspberry Pi and Lego](https://medium.com/seed-digital/how-to-sort-m-ms-using-raspberry-pi-computer-vision-and-lego-cb79bce22397)

This app is built in xcode using swift and cocoa pods.

You will need installed
    -Cocoa Pods
    -Xcode

To get started:

    $ pod install

after the command finishes running open "Lego-Sorter.xcworkspace" in Xcode.